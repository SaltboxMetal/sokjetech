<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * @property int current_series_id
 * @property Series series
 * @property int id
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function series()
    {
        return $this->hasMany(Series::class);
    }

    /**
     * @param int $seriesId
     */
    public function setCurrentSeries(int $seriesId)
    {
        /** @var User $user */
        $user = Auth::user();
        $user->current_series_id = $seriesId;
        $user->save();
    }

    public function currentSeries(): Series
    {
        /** @var User $user */
        $user = Auth::user();
        return Series::find($user->current_series_id);
    }
}
