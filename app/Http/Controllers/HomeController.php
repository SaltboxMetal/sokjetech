<?php

namespace App\Http\Controllers;

use App\Series;
use Illuminate\Contracts\Support\Renderable;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $series = Series::all();

        return view('index', [
            'series' => $series
        ]);
    }

    /**
     * @return Renderable
     */
    public function series(): Renderable
    {
        /** @var User $user */
        $user = Auth::user();
        $currentSeries = $user->currentSeries();

        return view('series', [
            'series' => $currentSeries
        ]);
    }

    /**
     * @param string $series_id
     * @return RedirectResponse
     */
    public function changeSeries(string $series_id)
    {
        /** @var User $user */
        $user = Auth::user();
        $user->setCurrentSeries((int) $series_id);

        return redirect()->route('series');
    }

    public function editSeries(int $seriesId = null): Renderable
    {
        /** @var User $user */
        $user = Auth::user();
        if ($seriesId) {
            $user->setCurrentSeries($seriesId);
            $series = Series::find($seriesId);
        } else {
            $series = new Series();
        }

        return view('edit_series', [
            'series' => $series
        ]);
    }
}
