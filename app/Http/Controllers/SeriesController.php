<?php

namespace App\Http\Controllers;

use App\Series;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeriesController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function editSeries(Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = Auth::user();
        $id = $request->get('id');
        $name = $request->get('name');
        $description = $request->get('description');

        if (!$id) {
            $series = new Series([
                'name' => $name,
                'description' => $description
            ]);

            $series->user()->associate($user);
            $series->save();

            $user->setCurrentSeries($series->id);
        } else {
            /** @var Series $series */
            $series = $user->currentSeries();

            $series->name = $name;
            $series->description = $description;

            $series->save();
        }

        return redirect()->route('series');
    }
}
