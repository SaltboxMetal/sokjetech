<?php

namespace App\Http\Middleware;

use App\Series;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnsSeries
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @return mixed
     */
    public function handle(Request $request)
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::user();
            $seriesId = $request->route('series_id');
            $series = Series::find($seriesId);

            if ($series !== null && $series->user_id === $user->id) {
                $user->setCurrentSeries($seriesId);
                return redirect()->route('series');
            } else {
                return redirect()->route('index');
            }
        } else {
            return redirect('index');
        }
    }
}
