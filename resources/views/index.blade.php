@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{ route('edit_series') }}" class="btn btn-primary mb-2">Create Series</a>
            <table class="table">
                <thead>
                    <tr>
                        <th>Series name</th>
                        <th>Series description</th>
                        <th>Owner</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($series as $individualSeries)
                        <tr>
                            <td>{{ $individualSeries->name }}</td>
                            <td>{{ $individualSeries->description }}</td>
                            <td>{{ $individualSeries->user->name }}</td>
                            <td>
                                <a href="{{ route('change_series', ['series_id' => $individualSeries->id]) }}">
                                    Select
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('edit_series', ['series_id' => $individualSeries->id]) }}">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
