@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h1>{{ $series->name }}</h1>
            <p>{{ $series->description }}</p>
        </div>
    </div>
@endsection
