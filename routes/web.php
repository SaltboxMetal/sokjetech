<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('index');

Route::get('/series', 'HomeController@series')->name('series');

Route::get('/change_series/{series_id}', 'HomeController@changeSeries')->name('change_series')->middleware('owns_series');

Route::get('/edit_series/{series_id?}', 'HomeController@editSeries')->name('edit_series');

Route::post('edit_series', 'SeriesController@editSeries')->name('save_edit_series');

Auth::routes();

